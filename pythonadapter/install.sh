#!/bin/bash

set -x

export PREFIX=${PREFIX:=/usr/local}

apt install -yq libboost-python-dev libboost-locale-dev

hg clone https://pep.foundation/dev/repos/pEpPythonAdapter/
cd pEpPythonAdapter
# wget https://gitlab.com/juga0/pEpPythonAdapter_git/-/raw/sync_docker/setup.py
python3 setup.py build_ext --prefix=$PREFIX
# python3 -c 'import pEp'
export LD_LIBRARY_PATH=$PREFIX/lib
# python3 setup.py install --prefix=$PREFIX
export PYTHONPATH=/pEpPythonAdapter/build/lib.linux-x86_64-3.7/
# python3 -c 'import pEp'
python3 setup.py install --prefix=$PREFIX
