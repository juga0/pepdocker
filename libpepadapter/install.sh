#!/bin/bash

set -x

export PREFIX=${PREFIX:=/usr/local}

mkdir -p $PREFIX

echo '--------Creating local.conf--------------'
hg clone https://pep.foundation/dev/repos/libpEpAdapter
cd libpEpAdapter
cat <<EOF > local.conf
PREFIX=$PREFIX

ENGINE_LIB=-L$PREFIX/lib
ENGINE_INC=-I$PREFIX/include
EOF
cat local.conf

echo '--------Compiling--------------'
make install  # PREFIX=$PREFIX
cd ..
rm -rf libpEpAdapter
